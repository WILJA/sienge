package com.sienge.transport.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sienge.transport.dto.PaymentDataDTO;
import com.sienge.transport.model.Payment;

@RestController
@RequestMapping("payments")
public class PaymentController {

	@PostMapping
	public void save(@RequestBody Payment payment){

	}

	@PutMapping("/{id}")
	public void pay(@RequestBody PaymentDataDTO paymentDataDTO){

	}
}
