package com.sienge.transport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliveryPaymentsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run( DeliveryPaymentsServiceApplication.class, args);
	}
}
