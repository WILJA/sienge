package com.sienge.transport.repository;

import org.springframework.data.repository.CrudRepository;

import com.sienge.transport.model.CreditCard;
import com.sienge.transport.model.Payment;

public interface CreditCardRepository extends CrudRepository<CreditCard, Integer> {

}
