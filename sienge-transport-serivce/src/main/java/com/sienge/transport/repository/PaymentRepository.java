package com.sienge.transport.repository;

import org.springframework.data.repository.CrudRepository;

import com.sienge.transport.model.Payment;

public interface PaymentRepository  extends CrudRepository<Payment, Integer> {

}
