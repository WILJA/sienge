package com.sienge.transport.model;

public enum PaymentStatus {
	PAID, PENDING;
}
