package com.sienge.transport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SiengeConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiengeConfigServerApplication.class, args);
	}
}
