package com.sienge.transport.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sienge.transport.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, String>{
	
	Authority findByName(String name);
	
}