package com.sienge.transport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class SiengeEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiengeEurekaServerApplication.class, args);
	}
}
